# Dable

A platform that indicates the accessibility of places. 
project was created for the purposes of Software Engineering course in 2022.

## User interface
 [Designs](https://www.figma.com/file/mf5dyOzRrlSKKHuoDSH3Qi/Dable-final?node-id=0%3A1)
## User requirements 
R-1	
The user must be able to view a place's details.

Functional
-	April 12th 2022, 07:53
R-2	
The user must be able to search places based on main place characteristics.

Functional
-	April 12th 2022, 07:53
R-3	
The user must be able to filter places based on secondary place characteristics.

Functional
-	April 12th 2022, 07:54
R-4	
The registered user must be able to evaluate a place.

Functional
-	April 12th 2022, 07:54
R-6	
The registered user must be able to delete their evaluation.

Functional
-	April 12th 2022, 07:55
R-7	
The registered user must be able to add a place to their favorites.

Functional
-	April 12th 2022, 07:55
R-8	
The registered user must be able to remove a place from their favorites.

Functional
-	April 12th 2022, 07:56
R-9	
The registered user must be able to view their favorites.

Functional
-	April 12th 2022, 07:57
R-10	
The registered user must be able to view recommended places.

Functional
-	April 12th 2022, 07:57
R-11	
The registered user must be able to give feedback to admins.

Functional
-	April 12th 2022, 07:57
R-13	
The system must have response time less than 0.5 seconds

Non Functional
-	April 24th 2022, 22:17
R-14	
The system must be secure from cyberattacks

Non Functional
-	April 24th 2022, 22:18
R-15	
The system must be compliant with GDPR

Non Functional
-	April 24th 2022, 22:19
R-16	
The system must be compliant with WCAG

Non Functional
-	April 24th 2022, 22:21
R-17	
The system must be reliable 95% of the time
## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Aspirenie/dable.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Aspirenie/dable/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
